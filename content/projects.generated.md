---
title: "Projects"
draft: false
slug: projects
---

Here's what I'm up to right now.
[Connect with me to stay in the loop!](/about#get-in-touch)

## Learning Journey

Ever since the start of my electronic journey, I have been fascinated with the
Internet and the concept of having my own website. Over the years I have looked
into various technologies, varying from PHP in 2016 to
[Express on node.js](https://expressjs.com) in 2018 to [Hugo](https://gohugo.io)
today.

Throughout my journey with web technologies, I have made many many projects that
I have just abandoned. My very first project was a
[Discord](https://discord.com) bot running on
[discord.js](https://discord.js.org). During the
[COVID-19 Pandemic](https://en.wikipedia.org/wiki/COVID-19_pandemic) I made the
switch to [Arch Linux](https://archlinux.org) and slowly learned how to use it.

I do have this habit of starting projects and then suddenly abandoning them,
which is unfortunate, and leads to a lot of lost time, but it helps me learn.
I am trying to commit everything to my GitLab page so I can share my code, and
post some things on my [blog](/blog) so I can talk about it in length.

## This Blog

On this website, I share my thoughts and notes about things that I’m excited
about and working on and hope to connect with people having a similar mindset.
I’d love to hear from you!

## My Homelab

Stay tuned! I'm going to plan it out here before I implement it.

## Open-Source Contributions

I just love to contribute to open-source projects,
[mostly on GitLab](https://gitlab.com/ailig).





- [Ailig Dòmhnallach pushed new project branch main at Ailig Dòmhnallach / ArchInst](https://gitlab.com/ailig/archinst/-/commits/main) (1 day ago)

- [Ailig Dòmhnallach created project Ailig Dòmhnallach / ArchInst](https://gitlab.com/ailig/archinst) (1 day ago)

- [Ailig Dòmhnallach pushed to project branch main at Ailig Dòmhnallach / ailig.uk](https://gitlab.com/ailig/ailig.uk/-/compare/0713bcdf04bb53ba55bdffd425ede69bdc6763fe...c0082a051f4c022fe9168c52a57f0bb53d29f6fa) (4 days ago)

- [Ailig Dòmhnallach pushed to project branch main at Ailig Dòmhnallach / ailig.uk](https://gitlab.com/ailig/ailig.uk/-/commit/0713bcdf04bb53ba55bdffd425ede69bdc6763fe) (4 days ago)

- [Ailig Dòmhnallach pushed to project branch main at Ailig Dòmhnallach / ailig.uk](https://gitlab.com/ailig/ailig.uk/-/commit/8ca222b9f9451abff9f683f65b01ed0efd24c073) (4 days ago)

- [Ailig Dòmhnallach pushed to project branch main at Ailig Dòmhnallach / ailig.uk](https://gitlab.com/ailig/ailig.uk/-/commit/a1fc1d40691861d0c447f93c07a0e70679352ca6) (4 days ago)

- [Ailig Dòmhnallach pushed new project branch main at Ailig Dòmhnallach / ailig.uk](https://gitlab.com/ailig/ailig.uk/-/commits/main) (4 days ago)

- [Ailig Dòmhnallach created project Ailig Dòmhnallach / ailig.uk](https://gitlab.com/ailig/ailig.uk) (5 days ago)

- [Ailig Dòmhnallach created project Ailig Dòmhnallach / Arch Installation Scripts](https://gitlab.com/ailig/arch-inst) (1 week ago)

- [Ailig Dòmhnallach pushed to project branch main at Ailig Dòmhnallach / website](https://gitlab.com/ailig/website/-/commit/02b80cc5b62188a7cf51eccb952deee9c25047f3) (2 weeks ago)




