---
title: "About"
date: 2023-08-27T01:33:34+01:00
---

When I was growing up in the early 2010s, I first got to use a MacBook at my
primary school. Since then, I have been fascinated by computers, data, and the
Internet, giving me access to a universe outside of the scope of the
understanding of a then 10-year-old. I've lived through the digital age, but I
didn't quite grow up with technology. Today I use the Internet every day, quite
like you; I even run [Arch Linux](https://archlinux.org) on my laptop,
**I use Arch btw**.

With infinite amounts of information at my fingertips, I love discovering and
learning new things every day. I enjoy building (and breaking) tech to
efficiently solve real-world problems. When I'm on my own time, I sometimes like
over-engineering things just because I can. :rocket:

On this website, I share my thoughts and notes about things that I'm excited
about and working on and hope to connect with people who have a similar mindset.
I'd love to [hear from you](#get-in-touch)!

## Get in Touch

Connect with me! Stay up-to-date with what I'm sharing and working on:

- Mastodon: <https://blahaj.zone/@TheAchlys>
- GitLab: <https://gitlab.com/ailig>
- Telegram: <https://t.me/ailig_domhnallach>
- E-mail: <dev@ailig.uk>
- RSS Feed: <https://ailig.uk/index.xml>

## Sponsoring

**I do not offer advertisements or sponsorships** for any form of payment. I
author all the content on this blog and offer it for free as in free beer. All
recommendations I make are independent and unbiased.

**I do not use affiliate links for anything I recommend**. Affiliate links are
specific web links that reward the advertiser with commissions for visits,
signups, or sales through that link.

**I do not accept guest posts or sponsored content in any form.** I ignore all
such requests, so please don't contact me about that.

**I do have a way to donate** if you enjoy the content here. You can
[buy me a coffee](https://buymeacoffee.com/ailig) to help me through my studies.

## Thanks ❤️

Thank you, dear reader! Thank you for taking the time to read my blog. Thank you
to everyone contributing to the open-source community. If it wasn't for you,
this website wouldn't exist.

These are the building blocks I built this website on:

<!-- markdownlint-disable MD033 -->

|                   |                                                 |
| ----------------- | ----------------------------------------------- |
| Analytics         | [Google Analytics]                              |
| Search            | [FlexSearch]                                    |
| Code Highlighting | [PrismJS]                                       |
| SVG Icon suites   | [Simple Icons] <br> [Tabler Icons]              |
| Toolchain         | [Hugo] <br> [esbuild] <br> [PostCSS] <br> [npm] |

[Google Analytics]: https://analytics.google.com
[FlexSearch]: https://github.com/nextapps-de/flexsearch
[PrismJS]: https://prismjs.com
[Simple Icons]: https://simpleicons.org
[Tabler Icons]: https://tablericons.com
[Hugo]: https://gohugo.io
[esbuild]: https://esbuild.github.io
[PostCSS]: https://postcss.org
[npm]: https://npmjs.com

<!-- markdownlint-enable MD033 -->

## Legal

The [repository](https://gitlab.com/ailig/ailig.uk) is licenced under
[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

All trademarks are the property of their respective owners.
