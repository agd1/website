module ailig.uk

go 1.21

require (
	github.com/schnerring/hugo-mod-json-resume v0.0.0-20230821101149-458ad0c10ccd // indirect
	github.com/schnerring/hugo-theme-gruvbox v0.0.0-20230826143656-29927cfb5a23 // indirect
)
