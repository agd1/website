---
title: Project Readme
url: /readme
---

<!-- markdownlint-disable MD025 -->

# ailig.uk

My website, [ailig.uk](https://ailig.uk).

![Build status](https://img.shields.io/gitlab/pipeline-status/agd1%2Fwebsite?logo=cloudflare)
![Website status](https://img.shields.io/website/https/ailig.uk?logo=cloudflare)
![Last commit](https://img.shields.io/gitlab/last-commit/49067098?logo=gitlab)
![Licence](https://img.shields.io/gitlab/license/49067098)

## Local development

Install npm dependencies:

```sh
npm ci
```

Run Hugo:

```sh
hugo server -D
```

## Licence

- The [repository](https://gitlab.com/agd1/website) is licenced under [CC BY-SA 4.0](./LICENCE)
