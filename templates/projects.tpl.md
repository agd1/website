---
title: "Projects"
draft: false
slug: projects
---

Here's what I'm up to right now.
[Connect with me to stay in the loop!](/about#get-in-touch)

## Learning Journey

Ever since the start of my electronic journey, I have been fascinated with the
Internet and the concept of having my own website. Over the years I have looked
into various technologies, varying from PHP in 2016 to
[Express on node.js](https://expressjs.com) in 2018 to [Hugo](https://gohugo.io)
today.

Throughout my journey with web technologies, I have made many many projects that
I have just abandoned. My very first project was a
[Discord](https://discord.com) bot running on
[discord.js](https://discord.js.org). During the
[COVID-19 Pandemic](https://en.wikipedia.org/wiki/COVID-19_pandemic) I made the
switch to [Arch Linux](https://archlinux.org) and slowly learned how to use it.

I do have this habit of starting projects and then suddenly abandoning them,
which is unfortunate, and leads to a lot of lost time, but it helps me learn.
I am trying to commit everything to my GitLab page so I can share my code, and
post some things on my [blog](/blog) so I can talk about it in length.

## This Blog

On this website, I share my thoughts and notes about things that I’m excited
about and working on and hope to connect with people having a similar mindset.
I’d love to hear from you!

## My Homelab

Stay tuned! I'm going to plan it out here before I implement it.

## Open-Source Contributions

I just love to contribute to open-source projects,
[mostly on GitLab](https://gitlab.com/ailig).

<!-- prettier-ignore-start -->
<!-- markdownlint-disable MD032 MD034 -->

{{range rss "https://gitlab.com/ailig.atom" 10}}
- [{{.Title}}]({{.URL}}) ({{humanize .PublishedAt}})
{{end}}

<!-- markdownlint-enable MD032 MD034 -->
<!-- prettier-ignore-end -->
