---
title: "{{ humanize .Name | title }}"
date: "{{ .Date }}"
draft: true
socialShare: true
toc: false
cover:
  src: cover.png
---
